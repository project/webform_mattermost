<?php

namespace Drupal\webform_mattermost\Form\AdminConfig;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformTokenManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Configure webform admin settings for Mattermost.
 */
class WebformAdminConfigMattermostForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['webform_mattermost.settings'];
  }

  /**
   * The webform token manager.
   *
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $tokenManager;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_admin_config_mattermost_form';
  }

  /**
   * Constructs a WebformAdminConfigMattermostForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\webform\WebformTokenManagerInterface $token_manager
   *   The webform token manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, WebformTokenManagerInterface $token_manager) {
    parent::__construct($config_factory);
    $this->tokenManager = $token_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('webform.token_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('webform_mattermost.settings');

    $form['message'] = [
      '#type' => 'details',
      '#title' => $this->t('Message settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    $form['message']['default_body_text'] = [
      '#type' => 'webform_codemirror',
      '#mode' => 'text',
      '#title' => $this->t('Default message body (Mattermost markdown)'),
      '#description' => $this->t('A wide range of rich text formatting options, including bold, italic, headings, in-line images, and tables, can be used in integrations. For more information about formatting, see https://docs.mattermost.com/help/messaging/formatting-text.html'),
      '#required' => TRUE,
      '#default_value' => $config->get('message.default_body_text'),
    ];
    $form['message']['token_tree_link'] = $this->tokenManager->buildTreeLink();

    $this->tokenManager->elementValidate($form);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('webform_mattermost.settings');
    $config->set('message', $form_state->getValue('message'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
