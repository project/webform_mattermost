<?php

namespace Drupal\webform_mattermost\Tests\Handler;

use Drupal\webform\Entity\Webform;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\Tests\WebformTestBase;

/**
 * Tests for webform mattermost handler functionality.
 *
 * @group webform_mattermost
 */
class WebformHandlerMattermostTest extends WebformTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'webform',
    'webform_mattermost',
    'webform_test_handler_mattermost',
  ];

  /**
   * Webforms to load.
   *
   * @var array
   */
  protected static $testWebforms = [
    'test_handler_mattermost',
  ];

  /**
   * Test remote post handler.
   */
  public function testMattermostHandler() {
    $this->drupalLogin($this->rootUser);

    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = Webform::load('test_handler_mattermost');

    // Check 'post' operation.
    $sid = $this->postSubmission($webform);

    // Check POST response.
    $this->assertRaw("method: post
status: success
message: 'Processed request.'
options:
  json:
    channel: test
    username: Test");

    $webform_submission = WebformSubmission::load($sid);

    // Check confirmation number is set via the
    // [webform:handler:remote_post:completed:confirmation_number] token.
    $this->assertRaw('Your confirmation number is ' . $webform_submission->getElementData('confirmation_number') . '.');

    // Switch anonymous user.
    $this->drupalLogout();
  }

}
