<?php

namespace Drupal\webform_mattermost\Plugin\WebformHandler;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * Sends a webform submission to Mattermost.
 *
 * @WebformHandler(
 *   id = "mattermost",
 *   label = @Translation("Post to Mattermost"),
 *   category = @Translation("Notification"),
 *   description = @Translation("Sends a webform submission to Mattermost."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class MattermostWebformHandler extends WebformHandlerBase {

  /**
   * The token manager.
   *
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $tokenManager;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->tokenManager = $container->get('webform.token_manager');
    $instance->httpClient = $container->get('http_client');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $mattermostSettings = $this->configFactory->get("webform_mattermost.settings");
    return [
      'webhook_url' => '',
      'channel' => '',
      'username' => '',
      'icon_url' => 'https://mattermost.org/wp-content/uploads/2016/04/icon.png',
      'message' => $mattermostSettings->get('message.default_body_text') ?? '',
      'debug' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    // Mattermost settings.
    $form['mattermost_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Mattermost settings'),
      '#open' => TRUE,
    ];
    $form['mattermost_settings']['webhook_url'] = [
      '#type' => 'url',
      '#title' => $this->t('URL'),
      '#description' => $this->t('Incoming webhook URL configured in Mattermost.'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['webhook_url'],
    ];
    $form['mattermost_settings']['channel'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Channel'),
      '#description' => $this->t('Overrides the channel the message posts in.'),
      '#default_value' => $this->configuration['channel'],
    ];
    $form['mattermost_settings']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#description' => $this->t('Overrides the username the message posts as.'),
      '#default_value' => $this->configuration['username'],
    ];
    $form['mattermost_settings']['icon_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ICON URL'),
      '#description' => $this->t('Provide the absolute path.'),
      '#default_value' => $this->configuration['icon_url'],
    ];

    // Message.
    $form['message'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Message settings'),
    ];

    $form['message']['message'] = [
      '#type' => 'webform_codemirror',
      '#title' => $this->t('Message body (Mattermost markdown)'),
      '#default_value' => $this->configuration['message'],
      '#required' => TRUE,
    ];

    // Development.
    $form['development'] = [
      '#type' => 'details',
      '#title' => $this->t('Development settings'),
    ];
    $form['development']['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debugging'),
      '#description' => $this->t('If checked, every handler method invoked will be displayed onscreen to all users.'),
      '#return_value' => TRUE,
      '#default_value' => $this->configuration['debug'],
    ];

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['webhook_url'] = $form_state->getValue('webhook_url');
    $this->configuration['channel'] = $form_state->getValue('channel');
    $this->configuration['username'] = $form_state->getValue('username');
    $this->configuration['icon_url'] = $form_state->getValue('icon_url');
    $this->configuration['message'] = $form_state->getValue('message');
    $this->configuration['debug'] = (bool) $form_state->getValue('debug');
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = FALSE) {
    $message = $this->configuration['message'];
    $message = $this->replaceTokens($message, $this->getWebformSubmission());
    $messageRawText = Markup::create(Xss::filter($message))->__toString();
    $this->sendMessage($messageRawText);
    $this->debug(__FUNCTION__);
  }

  /**
   * Post message to mattermost.
   *
   * @var string $message
   *   The message want to post to mattermost.
   */
  public function sendMessage($message) {
    if (empty($this->configuration['webhook_url'])) {
      return;
    }

    $request_url = $this->configuration['webhook_url'];
    $channel = $this->configuration['channel'];
    $username = $this->configuration['username'];
    $icon_url = $this->configuration['icon_url'];

    try {
      $payload = [
        'channel' => $channel,
        'username' => $username,
        'icon_url' => $icon_url,
        'text' => $message,
      ];

      $this->httpClient->request('POST', $request_url, [
        'headers' => [
          'Content' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'body' => json_encode($payload, JSON_FORCE_OBJECT),
      ]);
    }
    catch (RequestException $request_exception) {
      $this->loggerFactory->get('webform_mattermost')->error($request_exception->getMessage());
    }
  }

  /**
   * Display the invoked plugin method to end user.
   *
   * @param string $method_name
   *   The invoked method name.
   * @param string $context1
   *   Additional parameter passed to the invoked method name.
   */
  protected function debug($method_name, $context1 = NULL) {
    if (!empty($this->configuration['debug'])) {
      $t_args = [
        '@id' => $this->getHandlerId(),
        '@class_name' => get_class($this),
        '@method_name' => $method_name,
        '@context1' => $context1,
      ];
      $this->messenger()->addWarning($this->t('Invoked @id: @class_name:@method_name @context1', $t_args), TRUE);
    }
  }

}
