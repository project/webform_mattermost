<?php

namespace Drupal\webform_test_handler_mattermost;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Random;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

/**
 * Extend Guzzle client so that we can override remote posts.
 */
class WebformTestHandlerMattermostClient extends Client {

  /**
   * {@inheritdoc}
   */
  public function request($method, $uri = '', array $options = []) {
    if (strpos($uri, 'http://webform-test-handler-mattermost/') === FALSE) {
      return parent::request($method, $uri, $options);
    }

    if ($method == 'get') {
      parse_str(parse_url($uri, PHP_URL_QUERY), $params);
    }
    else {
      $params = (isset($options['json'])) ? $options['json'] : $options['form_params'];
    }

    $response_type = (isset($params['response_type'])) ? $params['response_type'] : 200;
    $random = new Random();
    // Handle 404 errors.
    switch ($response_type) {
      // 404 Not Found.
      case 404:
        return new Response(404, [], 'File not found');

      // 401 Unauthorized.
      case 401:
        $status = 401;
        $headers = ['Content-Type' => ['application/json']];
        $json = [
          'method' => $method,
          'status' => 'unauthorized',
          'message' => 'Unauthorized to process request.',
          'options' => $options,
        ];
        return new Response($status, $headers, Json::encode($json));

      // 500 Internal Server Error.
      case 500:
        $status = 500;
        $headers = ['Content-Type' => ['application/json']];
        $json = [
          'method' => $method,
          'status' => 'fail',
          'message' => 'Failed to process request.',
          'options' => $options,
        ];
        return new Response($status, $headers, Json::encode($json));

      // 200 OK.
      case 200:
      default:
        $status = 200;
        $headers = ['Content-Type' => ['application/json']];
        $json = [
          'method' => $method,
          'status' => 'success',
          'message' => 'Processed request.',
          'options' => $options,
          'confirmation_number' => $random->name(20, TRUE),
        ];
        $response = new Response($status, $headers, Json::encode($json));
        return $response;
    }
  }

}
