CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

 This module adds a handler for sending webform submissions to Mattermost.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/webform_mattermost

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/webform_mattermost

 * There is a video on installation, configuration, and first use:
   https://youtu.be/Fiu82WWwQY0

REQUIREMENTS
------------

This module requires the following modules:

 * Webform (https://www.drupal.org/project/webform)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/extending-drupal/installing-drupal-modules
   for further information.

CONFIGURATION
-------------

 * Add an incoming webhook in Mattermost by following steps 1, 2, and 3 of
   the Mattermost documentation for incoming webhooks:
   https://docs.mattermost.com/developer/webhooks-incoming.html
 * Add and configure a webform
 * Add "Post to Mattermost" handler under Settings -> E-mail/Handlers
 * Configure the handler
